
Maker is gene annotation pipeline produced by the Yandell Lab. Department of Human Genetics. University of Utah. https://www.yandell-lab.org/software/maker.html

Maker is installed on Huxley as a module. You can load the software with:
`module load maker-3.01.03`

After loading the module, run this command to generate pre-populated control file:
`maker -CTL`

After filling in the control files as needed, you can run maker with a PBS script. Below is an example of a PBS script that uses 20 cpu cores.

```
#!/bin/bash
#PBS -V
#PBS -N maker.ex1
#PBS -q batch
#PBS -S /bin/bash
#PBS -l ncpus=20
#PBS -l walltime=80:0:00
#PBS -e /nas4/CG2/$USER/maker/class08/maker.class08.elog2
#PBS -o /nas4/CG2/$USER/maker/class08/maker.class08.olog2

module load maker-3.01.03

cd /nas4/CG2/$USER/maker/class08/

# running maker with mpiexec on one node:
mpiexec --oversubscribe --bind-to none --mca btl_openib_want_fork_support 1 --mca mpi_warn_on_fork 0 --mca btl vader,tcp,self --mca btl_tcp_if_include ib0 -n 20 maker
```
